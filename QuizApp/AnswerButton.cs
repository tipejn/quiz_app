﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizApp
{
    public class AnswerButton: Button
    {
        public Answer Answer { get; set; }

        private readonly Color defaultBackColor = SystemColors.ButtonHighlight;

        public AnswerButton(int index, Point location, EventHandler eventHandler)
        {
            BackColor = defaultBackColor;
            FlatStyle = FlatStyle.Flat;
            Location = location;
            Name = "AnswerBtn" + index.ToString();
            Size = new Size(180, 50);
            TabIndex = index;
            UseVisualStyleBackColor = false;
            FlatAppearance.BorderSize = 0;
            Click += eventHandler;
        }

        public void ProcessColors()
        {
            ChangeButtonColor(Color.Yellow);

            if (Answer.IsCorrect)
                ChangeButtonColor(Color.LightGreen);
            else
                ChangeButtonColor(Color.Red);

            this.BackColor = defaultBackColor;
        }

        private void ChangeButtonColor(Color color)
        {
            this.BackColor = color;
            this.Refresh();
            System.Threading.Thread.Sleep(1500);
        }
    }
}
