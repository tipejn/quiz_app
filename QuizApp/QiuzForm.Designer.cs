﻿namespace QuizApp
{
    partial class QuizForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.lblScore = new System.Windows.Forms.Label();
            this.lblTimeRemain = new System.Windows.Forms.Label();
            this.lblQuestionContent = new System.Windows.Forms.Label();
            this.ucUserNameInputBox = new QuizApp.UserNameInputbox();
            this.RankingBoard = new QuizApp.RankingBoard();
            this.SuspendLayout();
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(374, 11);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(47, 13);
            this.lblScore.TabIndex = 7;
            this.lblScore.Text = "Score: 0";
            // 
            // lblTimeRemain
            // 
            this.lblTimeRemain.AutoSize = true;
            this.lblTimeRemain.Location = new System.Drawing.Point(23, 11);
            this.lblTimeRemain.Name = "lblTimeRemain";
            this.lblTimeRemain.Size = new System.Drawing.Size(97, 13);
            this.lblTimeRemain.TabIndex = 7;
            this.lblTimeRemain.Text = "Time remain: 00:00";
            // 
            // lblQuestionContent
            // 
            this.lblQuestionContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblQuestionContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblQuestionContent.Location = new System.Drawing.Point(26, 44);
            this.lblQuestionContent.Name = "lblQuestionContent";
            this.lblQuestionContent.Size = new System.Drawing.Size(395, 91);
            this.lblQuestionContent.TabIndex = 6;
            this.lblQuestionContent.Text = "QuestionContent";
            this.lblQuestionContent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ucUserNameInputBox
            // 
            this.ucUserNameInputBox.Location = new System.Drawing.Point(1, 1);
            this.ucUserNameInputBox.Name = "ucUserNameInputBox";
            this.ucUserNameInputBox.Size = new System.Drawing.Size(444, 301);
            this.ucUserNameInputBox.TabIndex = 0;
            // 
            // RankingBoard
            // 
            this.RankingBoard.Location = new System.Drawing.Point(1, 1);
            this.RankingBoard.Name = "RankingBoard";
            this.RankingBoard.Size = new System.Drawing.Size(444, 301);
            this.RankingBoard.TabIndex = 8;
            // 
            // QuizForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 301);
            this.Controls.Add(this.RankingBoard);
            this.Controls.Add(this.ucUserNameInputBox);
            this.Controls.Add(this.lblTimeRemain);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.lblQuestionContent);
            this.Name = "QuizForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QuizApp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Timer Timer;
        public System.Windows.Forms.Label lblScore;
        public System.Windows.Forms.Label lblTimeRemain;
        public System.Windows.Forms.Label lblQuestionContent;
        public UserNameInputbox ucUserNameInputBox;
        public RankingBoard RankingBoard;
    }
}

