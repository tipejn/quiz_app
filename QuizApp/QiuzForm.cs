﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace QuizApp
{
    public partial class QuizForm : Form
    {
        public AnswerButton btnAnswer1;
        public AnswerButton btnAnswer2;
        public AnswerButton btnAnswer3;
        public AnswerButton btnAnswer4;

        Quiz quiz;

        public QuizForm()
        {
            InitializeComponent();
            InitializeInputBox();
            InitializeButtons();
            this.RankingBoard.Hide();

            quiz = new Quiz(this);
        }

        private void btnNick_Click(object sender, EventArgs e)
        {
            quiz.ProcessUser();
        }

        private void AnswerBtn_Click(object sender, EventArgs e)
        {
            AnswerButton btn = (AnswerButton)sender;
            btn.ProcessColors();
            quiz.ProcessAnswer(btn.Answer);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            quiz.ProcessTimer();
        }

        private void InitializeInputBox()
        {
            this.ucUserNameInputBox.Location = new System.Drawing.Point(0, 0);
            this.ucUserNameInputBox.btnNick.Click += new System.EventHandler(this.btnNick_Click);
            this.AcceptButton = ucUserNameInputBox.btnNick;
        }

        private void InitializeButtons()
        {
            btnAnswer1 = new AnswerButton(1, new Point(26, 156), new EventHandler(this.AnswerBtn_Click));
            btnAnswer2 = new AnswerButton(2, new Point(236, 156), new EventHandler(this.AnswerBtn_Click));
            btnAnswer3 = new AnswerButton(3, new Point(26, 225), new EventHandler(this.AnswerBtn_Click));
            btnAnswer4 = new AnswerButton(4, new Point(236, 225), new EventHandler(this.AnswerBtn_Click));

            this.Controls.Add(btnAnswer1);
            this.Controls.Add(btnAnswer2);
            this.Controls.Add(btnAnswer3);
            this.Controls.Add(btnAnswer4);
        }
    }
}
