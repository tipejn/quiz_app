﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizApp
{
    class Quiz
    {
        QuestionManager questionManager;
        Question currentQuestion;
        Scorer scorer;
        QuizForm quizForm;
        UserManager userManager;
        User user;
        int counter;

        public Quiz(QuizForm form)
        {
            quizForm = form;
            questionManager = new QuestionManager();
            userManager = new UserManager();
            scorer = new Scorer();
        }

        public void NewGame()
        {
            questionManager.NewQuestionSet();
            ProcessQuestion();
        }

        public void ProcessQuestion()
        {
            quizForm.lblScore.Text = string.Format("Score: {0}", scorer.Points);

            currentQuestion = questionManager.NextQuestion();

            if (currentQuestion != null)
                ShowQuestion(currentQuestion);
            else
            {
                EndGame();
            }
        }

        public void EndGame()
        {
            user.AddQuizResult(scorer.Points);
            ShowRanking();
            ShowFinalMsg();
            //quizForm.Dispose();
        }

        public void ShowRanking()
        {
            List<User> bestUsers = userManager.GetBestUsers();

            foreach (User user in bestUsers)
            {
                ListViewItem item = new ListViewItem(user.UserName);
                item.SubItems.Add(user.BestResult.ToString());
                quizForm.RankingBoard.RankingList.Items.Add(item);
            }

            quizForm.RankingBoard.Show();
            quizForm.RankingBoard.BringToFront();

        }

        public void ProcessAnswer(Answer answer)
        {
            quizForm.Timer.Enabled = false;

            if (answer.IsCorrect)
                scorer.AddPoint();

            ProcessQuestion();
        }

        public void ProcessTimer()
        {
            if (counter > 0)
            {
                counter--;
                UpdateTimerLabel();
            }
            else
            {
                quizForm.Timer.Enabled = false;
                ProcessQuestion();
            }
        }

        public void ProcessUser()
        {
            string userName = quizForm.ucUserNameInputBox.txtNick.Text;

            user = userManager.GetUser(userName);

            if (user != null)
            {
                quizForm.ucUserNameInputBox.Hide();
                InitializeTimer();
                NewGame();
            }
        }

        private void ShowFinalMsg()
        {
            string caption = string.Format("Congratulations {0}", user.UserName);

            StringBuilder msg = new StringBuilder();

            if (scorer.Points > user.BestResult)
                msg.Append("New Record! ");

            msg.AppendFormat("You gained {0} point", scorer.Points);

            if (scorer.Points != 1)
                msg.Append('s');

            msg.Append('!');

            MessageBox.Show(msg.ToString(), caption);
        }

        private void ShowQuestion(Question question)
        {
            quizForm.lblQuestionContent.Text = question.Content;
            AnswerButton[] answerButtons = new AnswerButton[]
                {
                    quizForm.btnAnswer1,
                    quizForm.btnAnswer2,
                    quizForm.btnAnswer3,
                    quizForm.btnAnswer4
                };

            foreach (AnswerButton answerBtn in answerButtons)
            {
                answerBtn.Answer = questionManager.NextAnswer(currentQuestion);
                answerBtn.Text = answerBtn.Answer.Content;
            }

            RestartTimer();
        }

        private void InitializeTimer()
        {
            quizForm.Timer.Interval = 1000;
            quizForm.Timer.Start();
        }

        private void RestartTimer()
        {
            counter = 15;
            quizForm.Timer.Enabled = true;
            UpdateTimerLabel();
        }

        private void UpdateTimerLabel()
        {
            quizForm.lblTimeRemain.Text = string.Format("Remaining time: 00:{0:D2}", counter);
        }
    }
}
