﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace QuizApp
{
    class QuestionManager
    {
        List<IQuizItem> questions;
        DBConnection db;

        public void NewQuestionSet()
        {
            db = new DBConnection();

            questions = db.GetQuestions();
        }

        public Question NextQuestion()
        {
            if (questions.Count == 0)
                return null;

            return (Question)GetQuizItem(questions);
        }

        public Answer NextAnswer(Question question)
        {
            return (Answer)GetQuizItem(question.Answers);
        }

        IQuizItem GetQuizItem(IList<IQuizItem> items)
        {
            Random rnd = new Random();

            int index = rnd.Next(items.Count);

            IQuizItem item = items[index];

            items.Remove(item);

            return item;
        }
    }
}
