﻿namespace QuizApp
{
    partial class UserNameInputbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNick = new System.Windows.Forms.TextBox();
            this.lblNick = new System.Windows.Forms.Label();
            this.btnNick = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNick
            // 
            this.txtNick.Location = new System.Drawing.Point(175, 133);
            this.txtNick.Name = "txtNick";
            this.txtNick.Size = new System.Drawing.Size(100, 20);
            this.txtNick.TabIndex = 0;
            // 
            // lblNick
            // 
            this.lblNick.AutoSize = true;
            this.lblNick.Location = new System.Drawing.Point(208, 104);
            this.lblNick.Name = "lblNick";
            this.lblNick.Size = new System.Drawing.Size(32, 13);
            this.lblNick.TabIndex = 1;
            this.lblNick.Text = "Nick:";
            // 
            // btnNick
            // 
            this.btnNick.Location = new System.Drawing.Point(188, 169);
            this.btnNick.Name = "btnNick";
            this.btnNick.Size = new System.Drawing.Size(75, 23);
            this.btnNick.TabIndex = 2;
            this.btnNick.Text = "OK";
            this.btnNick.UseVisualStyleBackColor = true;
            // 
            // UserNameInputbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnNick);
            this.Controls.Add(this.lblNick);
            this.Controls.Add(this.txtNick);
            this.Name = "UserNameInputbox";
            this.Size = new System.Drawing.Size(444, 301);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtNick;
        public System.Windows.Forms.Label lblNick;
        public System.Windows.Forms.Button btnNick;
    }
}
