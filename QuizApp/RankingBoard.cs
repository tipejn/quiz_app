﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizApp
{
    public partial class RankingBoard : UserControl
    {
        public Label RankingLabel
        {
            get { return this.lblRanking; }
        }

        public ListView RankingList
        {
            get { return this.listRanking; }
        }

        public RankingBoard()
        {
            InitializeComponent();
        }
    }
}
