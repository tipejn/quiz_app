﻿
using System.Collections.Generic;

namespace QuizApp
{
    public class Question : IQuizItem
    {
        public int ID { get; set; }

        public string Content { get; set; }

        public List<IQuizItem> Answers { get; set; }
    }
}
