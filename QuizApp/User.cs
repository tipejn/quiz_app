﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizApp
{
    public class User
    {
        public int ID { get; set; }    

        public string UserName { get; set; }

        public int BestResult { get; set; }

        public void AddQuizResult(int points)
        {
            DBConnection db = new DBConnection();

            db.AddUserResult(ID, points);
        }
    }
}
