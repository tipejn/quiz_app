﻿
namespace QuizApp
{
    class Scorer
    {
        public int Points { get; private set; }

        public void AddPoint()
        {
            Points++;
        }

        public void Clear()
        {
            Points = 0;
        }
    }
}
