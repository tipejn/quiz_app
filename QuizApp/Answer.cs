﻿
namespace QuizApp
{
    public class Answer: IQuizItem
    {
        public int ID { get; set; }

        public string Content { get; set; }

        public bool IsCorrect { get; set; }
    }
}
