﻿
namespace QuizApp
{
    public interface IQuizItem
    {
        int ID { get; set; }

        string Content { get; set; }
    }
}
