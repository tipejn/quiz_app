﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizApp
{
    class UserManager
    {
        public User GetUser(string userName)
        {
            if (userName == string.Empty)
                return null;

            DBConnection db = new DBConnection();

            bool userExists = db.UserExists(userName);

            if (!userExists)
                db.AddUser(userName);

            return db.GetUser(userName);
        }

        public List<User> GetBestUsers()
        {
            DBConnection db = new DBConnection();

            return db.GetBestPlayers();
        }

    }
}
