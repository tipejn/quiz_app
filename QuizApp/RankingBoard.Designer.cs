﻿namespace QuizApp
{
    partial class RankingBoard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRanking = new System.Windows.Forms.Label();
            this.listRanking = new System.Windows.Forms.ListView();
            this.UserName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Result = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lblRanking
            // 
            this.lblRanking.AutoSize = true;
            this.lblRanking.Location = new System.Drawing.Point(35, 45);
            this.lblRanking.Name = "lblRanking";
            this.lblRanking.Size = new System.Drawing.Size(68, 13);
            this.lblRanking.TabIndex = 1;
            this.lblRanking.Text = "Best Players:";
            // 
            // listRanking
            // 
            this.listRanking.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listRanking.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UserName,
            this.Result});
            this.listRanking.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listRanking.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listRanking.Location = new System.Drawing.Point(38, 61);
            this.listRanking.Name = "listRanking";
            this.listRanking.Scrollable = false;
            this.listRanking.Size = new System.Drawing.Size(136, 155);
            this.listRanking.TabIndex = 2;
            this.listRanking.UseCompatibleStateImageBehavior = false;
            this.listRanking.View = System.Windows.Forms.View.Details;
            // 
            // UserName
            // 
            this.UserName.Text = "UserName";
            this.UserName.Width = 94;
            // 
            // Result
            // 
            this.Result.Text = "Result";
            this.Result.Width = 70;
            // 
            // RankingBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listRanking);
            this.Controls.Add(this.lblRanking);
            this.Name = "RankingBoard";
            this.Size = new System.Drawing.Size(444, 301);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblRanking;
        private System.Windows.Forms.ListView listRanking;
        private System.Windows.Forms.ColumnHeader UserName;
        private System.Windows.Forms.ColumnHeader Result;
    }
}
