﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace QuizApp
{
    public class DBConnection
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["QuizDB"].ConnectionString; }
        }

        public List<IQuizItem> GetQuestions()
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT QuestionID, Content FROM dbo.Questions";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        List<IQuizItem> questions = new List<IQuizItem>();

                        while (reader.Read())
                        {
                            Question question = new Question
                            {
                                ID = reader.GetInt32(0),
                                Content = reader.GetString(1),
                                Answers = GetAnswers(reader.GetInt32(0))
                            };

                            questions.Add(question);
                        }

                        return questions;
                    }
                }
            }
        }

        private List<IQuizItem> GetAnswers(int questionID)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT AnswerID, Content, IsCorrect FROM dbo.Answers WHERE QuestionID = @questionID";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.Add(new SqlParameter("@questionID", questionID));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        List<IQuizItem> answers = new List<IQuizItem>();

                        while (reader.Read())
                        {
                            Answer answer = new Answer
                            {
                                ID = reader.GetInt32(0),
                                Content = reader.GetString(1),
                                IsCorrect = reader.GetBoolean(2)
                            };

                            answers.Add(answer);
                        }

                        return answers;
                    }

                }
            }
        }

        public User GetUser(string userName)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT UserID FROM dbo.Users WHERE UserName = @userName";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.Add(new SqlParameter("@userName", userName));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                            return null;

                        User user = new User
                        {
                            ID = reader.GetInt32(0),
                            UserName = userName,
                            BestResult = GetUserBestResult(reader.GetInt32(0))
                        };

                        return user;
                    }
                }
            }
        }

        public int GetUserBestResult(int userID)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT MAX(Points) FROM dbo.UserHistory WHERE UserID = @userID";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.Add(new SqlParameter("@userID", userID));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();

                        if (reader.IsDBNull(0))
                            return 0;

                        return reader.GetInt32(0);
                    }
                }
            }
        }

        public void AddUser(string userName)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand("dbo.AddUser", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@userName", userName));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteUser(string userName)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "DELETE FROM dbo.Users WHERE UserName = @userName";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.Add(new SqlParameter("@userName", userName));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void AddUserResult(int userId, int points)
        {
            using(SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand("dbo.AddUserResult", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@userID", userId));
                    cmd.Parameters.Add(new SqlParameter("@points", points));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public bool UserExists(string userName)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT * FROM dbo.Users WHERE UserName = @userName";

                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.Add(new SqlParameter("@userName", userName));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                            return true;

                        return false;
                    }
                }
            }
        }

        public List<User> GetBestPlayers()
        {
            using(SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                string sql = "SELECT UserName, BestResult FROM dbo.vRankingList";

                using(SqlCommand cmd = new SqlCommand(sql, con))
                {
                    using(SqlDataReader reader = cmd.ExecuteReader())
                    {
                        List<User> bestPlayers = new List<User>();

                        while (reader.Read())
                        {
                            User user = new User
                            {
                                UserName = reader.GetString(0),
                                BestResult = reader.GetInt32(1)
                            };
                            bestPlayers.Add(user);
                        }

                        return bestPlayers;
                    }
                }
            }
        }
    }
}
