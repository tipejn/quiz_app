﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApp;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizAppTests
{
    [TestClass]

    public class DBConnectionTest
    {
        DBConnection db;

        [TestInitialize]

        public void TestSetUp()
        {
            db = new DBConnection();
        }

        [TestMethod]

        public void DownloadQuestions()
        {
            List<IQuizItem> questions = db.GetQuestions();

            foreach (Question question in questions)
            {
                Assert.AreNotEqual("", question.Content);
                Assert.AreEqual(4, question.Answers.Count);
            }
        }

        [TestMethod]

        public void CaseSensivity()
        {
            string lcaseName = "tipejn";
            string ucaseName = "Tipejn";

            User lcaseUser = db.GetUser(lcaseName);
            User ucaseUser = db.GetUser(ucaseName);

            Assert.AreEqual(lcaseUser.ID, ucaseUser.ID);
        }

        [TestMethod]

        public void AddAndDeleteUser()
        {
            string userName = "Tester";

            db.AddUser(userName);

            User user = db.GetUser(userName);

            Assert.IsNotNull(user);

            db.DeleteUser(userName);

            user = db.GetUser(userName);

            Assert.IsNull(user);
        }

        [TestMethod]

        public void CheckBestResult()
        {
            string userName = "tipejn";

            int expectedValue = 3;

            User user = db.GetUser(userName);

            Assert.AreEqual(expectedValue, user.BestResult);
        }

        [TestMethod]

        public void HowManyBestPlayers()
        {
            int expectedValue = 5;

            List<User> users = db.GetBestPlayers();

            Assert.AreEqual(expectedValue, users.Count);
        }
    }
}
